import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="qmah",
    version="0.0.1",
    license="AGPLv3",
    author="AWSOME core team",
    # author_email="",
    description="A quantitative module of avalanche hazard",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/avalanche-warning/snow-cover/postprocessing/qmah",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    install_requires=[
        "numpy",
        "matplotlib",
        "pandas",
        "xarray"
    ]
)
