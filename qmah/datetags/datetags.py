import sys
import os
import json
import configparser
import multiprocessing
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr

import nwp_provider as awnwp
from awset.domain_settings import get

import pdb

# cd ~snowpack/forecasts


def get_weather(domain, vstations_csv_file, start_datetime=None, cast_type='nowcast'):
    """Load data"""
    #TODO: this function is somewhat superfluous if cropping and time selection will be integrated into the nwp_provider!
    df_vstations = pd.read_csv(vstations_csv_file)
    # df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file'))
    # df_vstations = pd.read_csv("/opt/snowpack/gridded-chain/data/vstations/vstations-tromso-full-ext_kattfjordeidet_large.csv")
    ## Load datetag-file and extract latest datetags older than fc dataset / yesterday  to automatically set start_datetime
    ## Load nc ds of hourly weather
    ds_nwp = awnwp.open_dataset(domain, 'gridded-chain', cast_type, update_dataset=False, prefer_nc_over_db=True)

    if start_datetime is not None:
        ds_nwp = ds_nwp.sel(time=slice(start_datetime, None))
    
    ## Crop geographic extent (to save cpu; exact merging on df_vstations is done in postprocess_for_datetags())
    mask_x = xr.DataArray(ds_nwp['x'].isin(df_vstations['easting'].values), dims='x')
    mask_y = xr.DataArray(ds_nwp['y'].isin(df_vstations['northing'].values), dims='y')
    ds_nwp = ds_nwp.where(mask_x & mask_y, drop=True)
    
    return ds_nwp

def resample_weather(ds_nwp, meteo_vars, TA_thresh=1.2):
    """
    Resample a data set of NWP inputs to 24 and 72 hour accumulations of snow and rain fall.

    The routine computes snow and rain fall from the meteo variables {PSUM, PSUM_PH} or 
    {PSUM, TA} (using a static temperature threshold).

    Parameters:
        ds_nwp: xarray.dataset
            containing the relevant meteo variables, see description above.
        meteo_vars: list(str)
            see description above
        TA_thresh: float
            a static temperature threshold to differentiate snow from rain. 
            Only used if TA in meteo_vars. Unit of degrees Celsius, even if 
            ds_nwp uses units of Kelvin.
            context AWSOME: recommended to retrieve from snp.ini file listed in domain.xml
    Returns:
        ds_resampled: xarray.dataset
            containing the variables hn24, hn72, rain24, rain72
    TODO:
        this step could be done within SNOWPACK and written to the output SMET file?!
    """
    ## (1) retrieve snowfall and rainfall variables
    # meteo_vars
    if 'PSUM' in meteo_vars and 'PSUM_PH' in meteo_vars:
        ds_nwp['snowfall'] = ds_nwp['PSUM'] * (1 - ds_nwp['PSUM_PH'])
        ds_nwp['rainfall'] = ds_nwp['PSUM'] * ds_nwp['PSUM_PH']
    elif 'PSUM' in meteo_vars and 'TA' in meteo_vars:
        if ('units' in ds_nwp['TA'].attrs and ds_nwp['TA'].attrs['units'] == 'K') or \
            (ds_nwp['TA'].min().values > 100):
            ds_nwp['TA'] = ds_nwp['TA'] - 273.15
            ds_nwp['TA'].attrs['units'] = 'C'
        ds_nwp['snowfall'] = xr.where(ds_nwp['TA'] < TA_thresh, ds_nwp['PSUM'], 0)
        ds_nwp['rainfall'] = xr.where(ds_nwp['TA'] >= TA_thresh, ds_nwp['PSUM'], 0)

    # (2) resample the data to daily frequency by summing up the values for each day
    hn24 = ds_nwp['snowfall'].resample(time='D').sum(dim='time')
    rain24 = ds_nwp['rainfall'].resample(time='D').sum(dim='time')

    # (3) combine these into a new dataset
    ds_resampled = xr.Dataset({
        'hn24': hn24,
        'rain24': rain24,
        'hn72': hn24.rolling(time=3, center=False).sum(),
        'rain72': rain24.rolling(time=3, center=False).sum()
    })

    return ds_resampled

def aggregate_weather(wx_resampled: pd.DataFrame, 
                      quantiles: dict={'hn24': 0.75, 'hn72': 0.25, 'rain24': 0.75, 'rain72': 0.25}):
    """Aggregate resampled weather (24- & 72-hour values) by specific quantiles. Ideally, this is done 
    per region/pixel/subregion and per elevation band (outside of this function)!

    Parameters:
    -----------
    wx_resampled : pd.DataFrame
        A DataFrame containing resampled weather data, analogously to the xarray dataset returned by resample_weather(). 
        The DataFrame should have a 'date' column indicating the dates of the observations and additional columns 
        for weather variables (e.g., 'hn24', 'hn72', 'rain24', 'rain72') containing the observed values.

    quantiles : dict, optional
        A dictionary specifying the quantiles to compute for each weather variable. The keys should 
        match the column names in `wx_resampled`, and the values should be the desired quantiles 
        (between 0 and 1). Defaults are {'hn24': 0.75, 'hn72': 0.25, 'rain24': 0.75, 'rain72': 0.25}.

    Returns:
    --------
    wx_aggregated : pd.DataFrame
        A DataFrame with the aggregated weather data. The DataFrame has a 'date' column and one 
        column for each weather variable specified in the `quantiles` dictionary. Each column contains 
        the specified quantile value for the corresponding weather variable on each date.

    """

    for v in quantiles.keys():
        if wx_resampled[v].isna().all():
            raise ValueError(f"All records of {v} are NA.")
        
    unique_dates = sorted(wx_resampled['date'].unique())
    wx_aggregated = pd.DataFrame({'date': unique_dates})
    for var, quant in zip(quantiles.keys(), quantiles.values()):
        # Aggregate the data by date and calculate the specified quantile
        agg = wx_resampled.groupby('date')[var].quantile(quant, interpolation='linear').reset_index()
        agg.columns = ['date', var]
        wx_aggregated = wx_aggregated.merge(agg, on='date', how='left')

    return wx_aggregated


def derive_datetags(wx, 
                    human_datetags=None, 
                    snow_thresholds={'hn24_trace': 2, 'hn72_thresh': 10, 'hn72_increment': 5}, 
                    rain_thresholds={'rain24_trace': 5, 'rain72_thresh': 5, 'rain72_increment': 2},  
                    additional_tags={'n_days': 10, 'hn24_cumsum': 25}):
    """
    Analyzes (regionally aggregated and temporally resampled) weather data to derive date tags for snow and rain events. 
    The function identifies dates with substantial snowfall and rainfall based on specified thresholds and adds extra 
    date tags in cases of large gaps with little snowfall events that accumulate over time. It can also integrate 
    additional human-provided dates.

    These model-derived date tags represent regional markers for times when potential critical avalanche layers got buried. 
    Analogously to human date tags, these regional markers are primarily represented by the onset dates of pronounced storms
    with substantial snowfall, but these markers can also be dates when little but consistent snowfall amounts buried the snow surface.

    To establish the model-derived date tags, this function first searches for dry periods by identifying all days with less than a trace amount
    of snowfall or rain in the provided weather data. The date tags are defined by the start of substantial storm
    periods (i.e., day before substantial snowfall) following dry periods. However, since potentially critical layers can also be buried by 
    small amounts of snow over multiple days, the function inserts additional date tags in between these main storm periods if the non-storm 
    periods were sufficiently long and characterized by substantial accumulations of small daily snowfall amounts. 
    The exact rules and thresholds for establishing the model-derived date tags are described in detail in the appendix of the paper Herla et al (2023), 
    or can be viewed directly in the source code below.

    Limitations: Datetags might not be computed optimally in cases of rain events that melt thick parts of the surface. To improve for these cases,
    the routine could also consider snow height and couple the computation of rain tags versus snowfall tags (which runs decoupled in this version).

    Herla, F., Haegeli, P., Horton, S., and Mair, P.: A Large-scale Validation of Snowpack Simulations in Support of Avalanche Forecasting Focusing 
    on Critical Layers, EGUsphere [preprint], https://doi.org/10.5194/egusphere-2023-420, 2023.

    Parameters:
        wx (DataFrame): Weather data containing the columns {date, hn24, hn72, rain24, rain72}. 
            See functions resampled_weather and aggregate_weather for preparing the DataFrame.
            In addition to temporal resampling, the DataFrame ideally represents spatially aggregated information. 
        human_datetags (list, optional): Additional dates provided by human input, integrated into the derived tags.
        snow_thresholds (dict): Thresholds for identifying substantial snow storm events.
            - 'hn24_trace': Minimum daily snowfall (hn24) to identify potential onset days of storms that have less than trace amount of new snow.
            - 'hn72_thresh': Minimum accumulated snowfall over 72 hours that signifies a substantial snowfall event.
            - 'hn72_increment': Additional amount over the 72-hour threshold that must be exceeded for a following day to consider the timing
                as onset of the storm instead of as recession of storm.
        rain_thresholds (dict): Similar to snow_thresholds, but for rainfall.
        additional_tags (dict): Criteria for adding more date tags where gaps are too large between snowfall events.
            - 'n_days': Number of days considered a large gap, prompting an additional date tag.
            - 'hn24_cumsum': Minimum cumulative snowfall within the gap to consider adding a date tag.

    Returns:
        pandas.Series: Series of datetime64[ns] dates containing the resulting date tags.
    """

    window_slack = 5  # a slack time window of a few days for some specific rules
    # Pre-process WX data
    wx = wx.sort_values('date')

    # Snowfall processing
    r1_snow = wx[wx['hn24'] < snow_thresholds['hn24_trace']].index
    # r1_snow = r1_snow[r1_snow < len(wx)]
    # r1_snow_dates = wx.loc[r1_snow, 'date']  # helpful for debugging
    r23bol = [
        (wx.loc[r1_snow[i]+1:min(r1_snow[i]+5, r1_snow[i+1]-1 if i < len(r1_snow)-1 else len(wx)), 'hn72'].max() > snow_thresholds['hn72_thresh'] and
         wx.loc[r1_snow[i]+1:min(r1_snow[i]+5, r1_snow[i+1]-1 if i < len(r1_snow)-1 else len(wx)), 'hn72'].max() > wx.loc[r1_snow[i], 'hn72'] + snow_thresholds['hn72_increment'])
        for i in range(len(r1_snow))
    ]
    r_snow = r1_snow[r23bol]
    basetags_snow = pd.to_datetime(wx.loc[r_snow, 'date'])
    # Remove close base tags with insufficient snow
    if len(basetags_snow) > 1:
        n_days_between = basetags_snow.diff().dt.total_seconds() / (24*3600)
        n_days_between = n_days_between[1:].astype('int')  # <-- indexing correct here: pd.DataFrame.diff behaves different from np.diff!
        hn24_sum_between = np.array([
            wx.loc[(wx['date'] > basetags_snow.iloc[i]) & (wx['date'] < basetags_snow.iloc[i+1]), 'hn24'].sum()
            for i in range(len(n_days_between))
        ])
        to_remove = np.where((n_days_between < window_slack) & (hn24_sum_between < snow_thresholds['hn72_thresh']))[0] + 1 # remove next tag!
        basetags_snow = basetags_snow.drop(basetags_snow.index[to_remove])

    # Combine with human tags
    if human_datetags is not None:
        human_datetags = pd.to_datetime(human_datetags).dropna()
        # TODO: allow for 1-2 days of slack to not have redundant tags!
        tags_snow = pd.to_datetime(sorted(set(basetags_snow).union(set(human_datetags))))
    else:
        tags_snow = pd.to_datetime(sorted(set(basetags_snow)))

    # Add end of timeseries as a tag
    # tags_snow = np.append(tags_snow, wx['date'].max())

    # Add more tags if gaps are too large:
    # --> insert more datetags where they are too far apart and little snow amounts in between accumulate
    # --> don't count from tag to tag, but from storm's end of the first tag up to the next tag
    n_days_between = tags_snow.diff().total_seconds() / (24*3600)
    n_days_between = n_days_between[1:].astype('int')
    storm_end = []
    # Calculate storm end dates (first day of 0.2*maxHN24 or at most 6 days later)
    for j in range(len(n_days_between)):
        date_range = wx[(wx['date'] > tags_snow[j]) & (wx['date'] < min(tags_snow[j+1], tags_snow[j]+pd.Timedelta(days=window_slack)))]
        max_hn24_idx = date_range['hn24'].idxmax()
        max_hn24_day = date_range['date'].loc[max_hn24_idx]
        date_range_postmax = wx[(wx['date'] > max_hn24_day) & (wx['date'] < tags_snow[j]+pd.Timedelta(days=7))]
        next_drier_day = date_range_postmax[date_range_postmax['hn24'] < 0.2 * date_range.loc[max_hn24_idx, 'hn24']]['date'].min()
        if not pd.isna(next_drier_day):
            result_date = next_drier_day
        else:
            result_date = date_range_postmax.loc[date_range_postmax['hn24'].idxmin(), 'date']

        storm_end.append(result_date)

    # Calculate cumulative hn24 between storm ends and the next tag
    hn24_cum_between = np.array([
        wx.loc[(wx['date'] > storm_end[j]) & (wx['date'] < tags_snow[j+1]), 'hn24'].sum()
        for j in range(len(n_days_between))
    ])

    # Determine gaps based on threshold days and snow sums
    gaps = np.where((n_days_between > additional_tags['n_days']) & (hn24_cum_between > additional_tags['hn24_cumsum']))[0]
    # Filling additional tags for large gaps with little snow accumulation
    filler_tags = []
    for gap in gaps:
        n_filler_windows = int(min(np.round(hn24_cum_between[gap]/additional_tags['hn24_cumsum']), np.round(n_days_between[gap]/additional_tags['n_days'])))
        if n_filler_windows > 1:
            hn24 = np.array(wx.loc[(wx['date'] > storm_end[gap]) & (wx['date'] < tags_snow[gap+1]), 'hn24'])
            dates = np.array(wx.loc[(wx['date'] > storm_end[gap]) & (wx['date'] < tags_snow[gap+1]), 'date'])
            r1 = np.where(np.diff(np.array([False] + list(np.diff(hn24)>0) + [True]).astype('int'))>0)[0]  # finds all local minima
            r1 = r1[~np.isin(r1, np.array([1, len(hn24)-1]))]  # exclude first and last day of gap from possible options if they are minima
            r1 = r1[(hn24.cumsum()[r1-1] > 10) & (np.flip(np.flip(hn24).cumsum())[r1+1] > 10)]  # enforces min 10cm hn24 sum to outer basetags
            if len(r1) > 0:
                for added_tag_idx in range(1, n_filler_windows):  # i.e., one tag less than n_filler_windows
                    cost_days = r1 - (added_tag_idx*n_days_between[gap]/n_filler_windows)
                    cost_hn = hn24.cumsum()[r1] - (added_tag_idx*hn24_cum_between[gap]/n_filler_windows)
                    cost = abs(cost_days + cost_hn)
                    filler_tags.append(dates[r1[np.argmin(cost)]])
    tags_snow = np.sort(np.concatenate([tags_snow, pd.to_datetime(filler_tags)]))

    
    # Rain processing (same rules as above, adjusted thresholds)
    r1_rain = wx[wx['rain24'] < rain_thresholds['rain24_trace']].index
    r23bol = [
        (wx.loc[r1_rain[i]+1:min(r1_rain[i]+5, r1_rain[i+1]-1 if i < len(r1_rain)-1 else len(wx)), 'rain72'].max() > rain_thresholds['rain72_thresh'] and
         wx.loc[r1_rain[i]+1:min(r1_rain[i]+5, r1_rain[i+1]-1 if i < len(r1_rain)-1 else len(wx)), 'rain72'].max() > wx.loc[r1_rain[i], 'rain72'] + rain_thresholds['rain72_increment'])
        for i in range(len(r1_rain))
    ]
    r_rain = r1_rain[r23bol]
    basetags_rain = pd.to_datetime(wx.loc[r_rain, 'date'])

    # Combine snow and rain tags
    datetags = np.asarray(sorted(set(tags_snow).union(set(basetags_rain))), dtype='datetime64[D]')

    return datetags


def postprocess_for_datetags(ds_nwp, df_vstations, meteo_vars, aggregate_weather_by='region', 
                             TA_thresh=1.2, ncpus=multiprocessing.cpu_count()-2, outfile=None, 
                             polygons=None, epsg={"vstations": None, "polygons": None}):
    """
    Post-process snowpack simulations and corresponding NWP data for datetags.

    This function 
        1. resamples NWP data to 24 and 72h time windows,  
        2. aggregates the resampled weather data by regions, vstations, or custom polygons (TODO) and elevation bands, 
        3. computes the datetags for each spatial unit,
        4. and stores the datetags in a CSV file. 
    Steps 2 and 3 are processed in parallel using multiple CPUs.

    Parameters:
    -----------
    ds_nwp : xarray.Dataset
        An xarray dataset containing NWP data used to force the snow cover model.

    df_vstations : pd.DataFrame
        A DataFrame containing virtual station information. Expected columns include 'vstation', 
        'easting', 'northing', 'region', 'band', and optionally 'error'. If an 'error' column is present, 
        only rows with 'error' == 0 are processed. Easting/northing are expected to be in the same CRS
        as given in ds_nwp.

    meteo_vars : list of str
        List of meteorological variables to consider for resampling and aggregation. These should 
        match the variable names in the `ds_nwp` dataset.

    aggregate_weather_by : str, optional
        Specifies the level of aggregation. Can be 'region', 'vstation', or 'polygon'. 
        - 'region': Aggregate by region and elevation band.
        - 'vstation': Aggregate by virtual station and elevation band. This is useful for semi-distributed 
                      modeling configurations where each vstations corresponds to a larger spatial area.
        - 'polygon': Aggregate by external polygon and elevation band. See also argument `polygons`
    TA_thresh : float, optional
        Temperature threshold in units of °C to distinguish between rain and snow. Used during resampling to separate 
        precipitation phase. Default is 1.2°C.

    ncpus : int, optional
        Number of CPU cores to use for parallel processing. Default is all available cores minus two.

    outfile : str, optional
        If provided, the output dictionary will be saved as a json file to the specified path.
    polygons : str, optional
        Path to the GeoJSON file containing the polygon geometries for aggregation. Required if `aggregate_weather_by` is 'polygon'.
        Note that the polygon id (or scalar index if no id available) needs to be added to the vstations csv file for further qmah
        post-processing if the datetags were computed based on external polygons.
    epsg : dict, optional
        Dictionary containing EPSG codes for the CRS of the 'vstations' and 'polygons'.
        If a value is None, it assumes the CRS is correctly set in the respective data.
        E.g., 4326 for lat-lon coordinates; vstations are mostly given in easting-northing coordinates!


    Returns:
    --------
    datetag_dict : dict
        A dictionary containing the datetags for each spatial unit and elevation band.
    """
    
    if 'error' in df_vstations.columns:
        df_vstations = df_vstations[df_vstations['error'] == 0]
    
    ## initial weather manipulation
    ds_resampled = resample_weather(ds_nwp, meteo_vars, TA_thresh=TA_thresh)
    wx_resampled = ds_resampled.to_dataframe().reset_index()
    wx_resampled.rename(columns={'x': 'easting', 'y': 'northing', 'time': 'date'}, inplace = True)
    wx_resampled = pd.merge(wx_resampled, df_vstations[['vstation', 'easting', 'northing', 'region_id', 'band']], on=['easting', 'northing'], how='right')
    # wx_resampled.to_csv('/opt/snowpack/qmah/tmp/wx_datetags/WXresampled_kattfjordeidet_large.csv', index = False)
    # wx_resampled = pd.read_csv('/opt/snowpack/qmah/tmp/wx_datetags/WXresampled_kattfjordeidet_large.csv')
    ## Parallelizable from here
    if aggregate_weather_by == 'region':
        combi_names = ['region_id', 'band']
    elif aggregate_weather_by == 'vstation':
        combi_names = ['vstation', 'band']
    elif aggregate_weather_by == 'polygon':
        if polygons is None:
            raise ValueError("polygons must be provided when aggregating by 'polygon'.")
        
        gdf_polygons = gpd.read_file(polygons)
        gdf_vstations = gpd.GeoDataFrame(df_vstations, geometry=gpd.points_from_xy(df_vstations.easting, df_vstations.northing))
        if epsg["vstations"] is not None:
            gdf_vstations.set_crs(epsg["vstations"], inplace=True)
        if epsg["polygons"] is not None:
            gdf_polygons.set_crs(epsg["polygons"], inplace=True)
        if gdf_polygons.crs != gdf_vstations.crs:
            gdf_vstations = gdf_vstations.to_crs(gdf_polygons.crs)

        # Spatial join to find which polygon each vstation belongs to
        joined = gpd.sjoin(gdf_vstations, gdf_polygons, how='left', predicate='within')
        df_vstations = df_vstations.loc[joined.index, :]
        if 'id' in gdf_polygons.columns:
            df_vstations.loc[:, 'polygon'] = joined['id']
        else:
            df_vstations.loc[:, 'polygon'] = joined.index_right
        df_vstations.dropna(subset=['polygon'], inplace=True)
        
        # Merge back to resampled weather data
        wx_resampled = pd.merge(wx_resampled, df_vstations[['polygon', 'vstation']], on='vstation', how='right')
        combi_names = ['polygon', 'band']
    else:
        raise ValueError("aggregate_weather_by must be one of 'region', 'vstation', or 'polygon'.")
    
    unique_combinations = wx_resampled[combi_names].drop_duplicates()
    args_list = [(ii, combi.tolist(), combi_names, wx_resampled) for ii, combi in unique_combinations.iterrows()]

    results = []
    # print('starting processing spatial unit')
    with multiprocessing.Pool(processes=ncpus) as pool:
        results = pool.starmap(_process_spatial_unit, args_list)

    datetags_dict = {
        "meta": {
            "level1": combi_names[0],
            "level2": combi_names[1]
        }
    }
    for result in results:
        if result[combi_names[0]] not in datetags_dict:
            datetags_dict[result[combi_names[0]]] = {}
        datetags_dict[result[combi_names[0]]][result[combi_names[1]]] = result['datetags'].astype('str').tolist()
    if outfile:
        with open(outfile, 'w') as json_file:
            json.dump(datetags_dict, json_file, indent=4)
    
    return datetags_dict
    

def _process_spatial_unit(i, combi, combi_names, wx_resampled):
    wx_filtered = wx_resampled[(wx_resampled[combi_names[0]] == combi[0]) & (wx_resampled[combi_names[1]] == combi[1])]
    wx_aggregated = aggregate_weather(wx_filtered, quantiles={'hn24': 0.5, 'hn72': 0.5, 'rain24': 0.5, 'rain72': 0.5})
    dtags = derive_datetags(wx_aggregated)
    return {combi_names[0]: combi[0], combi_names[1]: combi[1], 'datetags': dtags}


def assign_ddates_to_datetags(ddates, datetags):
    """
    This routine assigns each layer of a simulated snow profile (represented by their deposition dates) to a precomputed datetag. 
    The routine picks the youngest datetag that is older than the layer's deposition date. If no such datetag exists, 
    'NaT' (Not-a-Time) is assigned; tip: to test for NaT datetags, use np.isnan()!

    Parameters:
        ddates (np.ndarray): Array of layers' deposition dates, dtype must be coercible to datetime64[D]
        datetags (np.ndarray): Array of precomputed datetags, dtype must be coercible to datetime64[D]

    Returns:
        dtags_out (np.ndarray): Array of assigned datetags, same shape as ddates. dtype is str, but at this point still coercible to datetime64.

    TODO:
        Test whether routine works correctly!

    Example:
    --------
        >>> import numpy as np
        >>> ddates = np.array(['2024-01-05', '2024-01-10', '2024-01-15'], dtype='datetime64[D]')
        >>> datetags = np.array(['2024-01-01', '2024-01-07', '2024-01-14'], dtype='datetime64[D]')
        >>> assign_ddates_to_datetags(ddates, datetags)
        array(['2024-01-07', '2024-01-14', 'NaT'], dtype='datetime64[D]')
    """
    ddates = np.asarray(ddates, dtype='datetime64[D]')
    datetags = np.asarray(datetags, dtype='datetime64[D]')

    # Ensure datetags are sorted
    datetags_sorted = np.sort(datetags)
    
    # Find the indices where elements of ddates should be inserted to maintain order in datetags_sorted
    indices = np.searchsorted(datetags_sorted, ddates, side='left')
    
    # Select the youngest datetag that is older than each ddate
    dtags_out = np.empty(ddates.shape, dtype='datetime64[D]')
    valid_mask = indices < len(datetags_sorted)
    dtags_out[valid_mask] = datetags_sorted[indices[valid_mask]]
    dtags_out[~valid_mask] = np.datetime64('NaT')  # Handle cases where no valid datetag is found
    
    return dtags_out.astype('str')
    

if __name__ == '__main__':
    
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        sys.exit("[E] Synopsis in the context of AWSOME: python3 datetags.py domain outfile [ncpus]")

    domain = sys.argv[1]
    outfile = sys.argv[2]

    configfile = os.path.expanduser(f"~snowpack/gridded-chain/input/runtime_{domain}.ini")
    config = configparser.ConfigParser()
    config.read(configfile)
    df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file'))
    cast_type = 'nowcast'
    ds_nwp = get_weather(domain, config.get('Paths', '_vstations_csv_file'), cast_type=cast_type)

    config_snp = configparser.ConfigParser(delimiters=("="))
    config_snp.read(get(['gridded-chain', 'snowpack', 'snp_ini_file'], domain, config.get('Paths', '_domain_path')))
    TA_thresh = config_snp.getfloat('SnowpackAdvanced', 'THRESH_RAIN')
    meteo_vars = get(config.get('Paths', f'_xml_path_meteo_{cast_type}').split('/') + ['variables'], domain, config.get('Paths', '_domain_path'), throw=True).split()

    if len(sys.argv) == 4:
        ncpus = sys.argv[3]
        _ = postprocess_for_datetags(ds_nwp, df_vstations, meteo_vars, TA_thresh=TA_thresh, ncpus=ncpus, outfile=outfile, aggregate_weather_by='vstation')
    else:
        _ = postprocess_for_datetags(ds_nwp, df_vstations, meteo_vars, TA_thresh=TA_thresh, outfile=outfile, aggregate_weather_by='vstation')
    


    # TODO: what about harmonizing datetags?

    ## Deprecated idea:
    # """Move stencil across grid"""
    # for i, j in zip(df['i'], df['j']):
    #     pass
    #     ## aggregate into percentiles per stencil window

