import numpy as np
from . import instability_depth
from .problem_mapping import problem_mapping
from .. import instability


import pdb

def compute_hazard_chart_element(simplified_stratigraphy: dict, 
                                 datetags_pwl: list[str]|None=None, datetags_wet: list[str]|None=None,
                                 problem_types=["new_snow", "persistent_weak_layers", "wet_snow"]):
    """
    Compute hazard chart elements for specified subset of datetags.

    Parameters:
    simplified_stratigraphy (dict): Dictionary containing simplified stratigraphy data for different aspects, see example.json structure.
    datetags_pwl (list[str]): List of datetags ("YYYY-MM-DD") for persistent weak layers to filter by. If None, no filtering is applied.
    datetags_wet (list[str]): List of datetags ("YYYY-MM-DD") for wet snow layers to filter by. If None, no filtering is applied.
    problem_types (list[str]): List of problem types to consider, e.g., ["new_snow", "persistent_weak_layers", "wet_snow"].

    Returns:
    hazardChartElements (dict): See example_mod.json structure
    """
    hce = {}
    for aspect, stabindexes in simplified_stratigraphy.items():
        hce[aspect] = compute_hazard_chart_element_per_aspect(stabindexes, datetags_pwl, datetags_wet, problem_types)
    return hce


def compute_hazard_chart_element_per_aspect(simplified_stratigraphy_per_aspect, 
                                            datetags_pwl: list[str]|None=None, datetags_wet: list[str]|None=None,
                                            problem_types=["new_snow", "persistent_weak_layers", "wet_snow"]):
    """Subfunction -- See compute_hazard_chart_element() for information"""
    hce_aspect = {problem_type: {} for problem_type in problem_types}
    for problem_type in problem_types:
        for stabindex, dtag_dicts in simplified_stratigraphy_per_aspect.items():
            if stabindex in problem_mapping[problem_type]["stabilityIndices"]:
                stability = []
                depth = []
                gtypes = []
                datetags = []

                """Filter for desired datetags"""
                if problem_type == "new_snow":
                    filtered_dtag_dicts = {dtag: data for dtag, data in dtag_dicts.items() if dtag in "new_snow"}
                elif problem_type == "persistent_weak_layers":
                    if datetags_pwl is not None:
                        filtered_dtag_dicts = {dtag: data for dtag, data in dtag_dicts.items() if dtag in datetags_pwl}
                    else:
                        filtered_dtag_dicts = dtag_dicts
                elif problem_type == "wet_snow":
                    if datetags_wet is not None:
                        dtag_dicts = {dtag: data for dtag, data in dtag_dicts.items() if dtag in datetags_wet}
                    else:
                        filtered_dtag_dicts = dtag_dicts
                else:
                    raise ValueError("Unknown avalanche problem type.")
                
                if filtered_dtag_dicts:
                    """Get stability index characteristics for each desired datetag and assemble hazardChartElement"""
                    for dtag, dtag_dict in dtag_dicts.items():
                        stability.append(dtag_dict["value"])
                        depth.append(dtag_dict["depth"])
                        gtypes.append(dtag_dict["grainType"])
                        datetags.append(dtag)

                    # Compute weakest stability, deepest unstable depth, and corresponding gtypes, datetags
                    stability = np.array(stability, dtype=float)  # ensure all None arrays are handled properly with NaNs 
                    elem_stab, elem_depth, elem_gtypes, elem_dtags = \
                        instability_depth.pick_stability_and_depth(instability.weak_means[stabindex], 
                                                                instability.thresholds[stabindex], 
                                                                np.array(stability), np.array(depth), 
                                                                gtype=gtypes, datetag=datetags)
                    # Confirm problem types with grain type checks
                    
                    if elem_gtypes is not None:
                        problem_type_confirmed = None
                        if problem_type == "new_snow":
                            if all([gt[:2] in problem_mapping[problem_type]["grainTypes"] for gt in elem_gtypes]):
                                problem_type_confirmed = problem_type
                        elif problem_type in ["persistent_weak_layers", "wet_snow"]:
                            if any([gt[:2] in problem_mapping[problem_type]["grainTypes"] for gt in elem_gtypes]):
                                problem_type_confirmed = problem_type
                        
                        if problem_type_confirmed:
                            hce_aspect[problem_type_confirmed][stabindex] = {
                                    "value": elem_stab,
                                    "depth": elem_depth,
                                    "instabilityDrivenBy": elem_dtags[0],
                                    "depthDrivenBy": elem_dtags[1]
                                }
                    else:
                        # In this case, the computation of a stability index might have failed and resulted in all Nones
                        hce_aspect[problem_type][stabindex] = {
                                "value": elem_stab,
                                "depth": elem_depth,
                                "instabilityDrivenBy": elem_dtags,
                                "depthDrivenBy": elem_dtags
                            }
                    
    return hce_aspect