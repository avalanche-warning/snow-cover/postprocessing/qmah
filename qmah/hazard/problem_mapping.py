problem_mapping = {
    "new_snow": {
        "grainTypes": ["PP", "DF"],
        "stabilityIndices": ["Punstable", "Punstable_natural", "sk38_rta", "ccl", "time_to_failure"] 
    },
    "persistent_weak_layers": {
        "grainTypes": ["FC", "DH", "SH"],
        "stabilityIndices": ["Punstable", "Punstable_natural", "sk38_rta", "ccl"] 
    },
    "wet_snow": {
        "grainTypes": ["MF"],
        "stabilityIndices": ["Punstable_wet_natural", "lwc_index"]
    }
}