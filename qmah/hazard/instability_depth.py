import itertools
import numpy as np
from .. import instability
import pdb

def extract_stability_and_depth(prof, key, 
                                weak_means=instability.weak_means, thresholds=instability.thresholds, relevant_gtypes=instability.relevant_gtypes,
                                subset:str|None=None, 
                                add_gtype:bool=False):
    """
    Calculate the value of the profile's most unstable layer and the depth of the deepest unstable layer. This task can be computed
    for the entire snow column or for subsets of layers (such as specific datetags only), see Parameters and Example.

    Important: The profile layers need to be arranged so that ground level is at the start of the array, 
    snow surface level is at the end of the array (i.e., snowpacktools.snowpro format)

    Parameters:
        prof (dict): Snowpro profile dictionary containing 'key' and 'height'.
        key (str): The name of the stability index as used in prof
        weak_means (min/max): Is the weakest stability index value the maximum or the minimum?
        thresholds (dict): Dictionary containing the instability threshold for `key`.
        subset (str or None): A boolean expression as a string to filter `prof[key]` for specific layers. 
                              If None, no filtering is applied.
        add_gtype (bool): Also return the unique grain types of the weakest and deepest-unstable layer? Will be nan for stable layers.

    Returns:
        dict: A dictionary with the `value` of the most unstable layer and the `depth` of the deepest unstable layer.
              If no layer is unstable, returns the depth of the weakest layer.
              If add_gtype, the dict also contains `grainType` (list[str], or nan for stable layers)

    Examples:
        >>> layer_filter = 'np.isin(prof["datetag"], np.array(["2024-01-01", "2024-01-17"]))'
        >>> result = get_relevant_index_and_depth(prof, key="rta", 'max', subset=layer_filter)
    """

    thresh = thresholds[key]
    weak_means = weak_means[key]
    stability = prof[key]
    heights = prof["height"]
    hs = prof["height"][-1]
    gtypes = [grains[:2] for grains in prof["graintype"]]

    if subset is not None:
        mask = eval(subset)
        stability = stability[mask]
        heights = heights[mask]
        gtypes = [gtype for gtype, m in zip(gtypes, mask) if m]

    if len(heights) > 0:
        depths = hs - heights
    
        if add_gtype:
            stability_weakest, depth, gtype = pick_stability_and_depth(weak_means, thresh, stability, depths, gtypes)
            if stability_weakest is not None and not np.isin(np.array(gtype), np.array(relevant_gtypes[key])).any() and \
                (stability_weakest >= thresh if weak_means=='min' else stability_weakest <= thresh):
                # discard result when stable and grain type is not consistent with stability index
                # keep result though when stability_weakest is None (-> highlight issues with computing stability indices)
                out = None
            else:
                out = {"value": stability_weakest, "depth": depth, "grainType": gtype}
                
        else:
            stability_weakest, depth = pick_stability_and_depth(weak_means, thresh, stability, depths)
            out = {"value": stability_weakest, "depth": depth}
    else:
        out = None
    
    return out
    

def pick_stability_and_depth(weak_means, thresh, stability, depth, gtype=None, datetag=None):
    """
    Determine the weakest stability index and depth of the deepest unstable or weakest stable layer.

    Important: The arrays with the layer properties need to be arranged so that ground level is at the start of the array, 
    snow surface level is at the end of the array (i.e., snowpacktools.snowpro format)

    Parameters:
    -----------
    weak_means: str 
        Method to determine the weakest stability. It can be 'min' to find the minimum 
        stability value or 'max' to find the maximum stability value.
    thresh: float
        Threshold value to determine unstable layers. Stability values are compared against
        this threshold to find unstable indices.
    stability: np.ndarray
        Array of stability values corresponding to different layers.
    depth: np.ndarray
        Array of depth values corresponding to different layers. Must be the same length as `stability`.
    gtype: list, np.ndarray
        Array of grain types corresponding to different layers. Must be the same length 
        as `stability` and `depth`, but can have multiple grain types per list element, see example below. Default is None.
    datetag: list, np.ndarray
        Array of datetags corresponding to different layers. Must be the same length 
        as `stability` and `depth`. Default is None.

    Returns:
    --------
    tuple  
        - stability_weakest (float): The weakest stability value determined by the method specified in `weak_means`.  
        - depth_out (float): The depth of the deepest unstable layer if any unstable layers exist, 
                             otherwise the depth at the index of the weakest stability.  
        - gtype_out (list[str], optional): A list of unique, weakest and -if existant- deepest-unstable grain types.
                                           This is included only if `gtype` is provided.
        - datetag_out (tuple[str], optional): A tuple of two datetags, the first one related to the weakest stability,
                                              the second one related to depth_out. This is included only if `datetag` is provided.

    Raises:
    -------
    ValueError: If `weak_means` is not 'min' or 'max'.

    Example:
    --------
    >>> stability = np.array([0.3, 0.7, 0.2, 0.5])
    >>> depth = np.array([40, 30, 20, 10])
    >>> gtype = np.array(["FC", "RG", "SH", "DF"])
    >>> pick_stability_and_depth('min', 0.4, stability, depth, gtype)
    (0.2, 40, ['FC', 'SH'])
    >>> pick_stability_and_depth('min', 0.1, stability, depth, gtype)
    (0.2, 20, nan)
    >>> pick_stability_and_depth('max', 0.6, stability, depth)
    (0.7, 30)
    >>> gtype = [["FC", "DH"], "RG", "SH", "DF"]
    >>> pick_stability_and_depth('min', 0.4, stability, depth, gtype)
    """
    if np.isnan(stability).all():
        stability_weakest = None
        depth_out = None
        gtype_out = None
        datetag_out = None
        tuple_out = (None, None)
    else:
        if weak_means == 'min':
            weakest_index = np.nanargmin(stability)
            unstable_indices = np.nonzero(stability <= thresh)[0]  # deepest first
        elif weak_means == 'max':
            weakest_index = np.nanargmax(stability)
            unstable_indices = np.nonzero(stability >= thresh)[0]  # deepest first
        else:
            raise ValueError("Unknown value for 'weak_means'")
        
        if unstable_indices.size > 0:
            depth_out = depth[unstable_indices[0]]
            if gtype is not None: 
                if isinstance(gtype, np.ndarray):
                    gtype_out = np.unique(gtype[[weakest_index, unstable_indices[0]]]).tolist()
                else:
                    gtype_out_nested_list = [gtype[weakest_index], gtype[unstable_indices[0]]]
                    gtype_out = np.unique(np.array(list(_flatten(gtype_out_nested_list)))).tolist()
            if datetag is not None:
                datetag_out = (datetag[weakest_index], datetag[unstable_indices[0]])
        else:
            depth_out = depth[weakest_index]
            if gtype is not None: 
                if isinstance(gtype, np.ndarray):
                    gtype_out = np.unique(gtype[weakest_index]).tolist()
                else:
                    gtype_out = np.unique(np.array(list(_flatten(gtype[weakest_index])))).tolist()
            if datetag is not None:
                datetag_out = (datetag[weakest_index], datetag[weakest_index])
        
        stability_weakest = stability[weakest_index]
        tuple_out = (np.round(stability_weakest, 2), np.round(depth_out, 2))
    
    if gtype is not None:
        tuple_out += (gtype_out,)
    if datetag is not None:
        tuple_out += (datetag_out,)
    
    return tuple_out

def _flatten(lst):
    """Helper function to flatten a nested list."""
    for item in lst:
        if isinstance(item, list):
            yield from _flatten(item)
        else:
            yield item