import sys
import os
import time
import json
import multiprocessing as mp
import copy
import argparse

import numpy as np
import pandas as pd
from datetime import datetime, timedelta

from qmah import instability
from qmah import hazard
from qmah.datetags import datetags
import snowsim_provider as awsnp
from snowpacktools.snowpro import snowpro
from snowpacktools.snowpro.instability_rfm_mayer import calc_punstable

import pdb

date_format       = '%Y-%m-%d'
datetime_format   = '%Y-%m-%dT%H:%M'
datetime_format_2 = '%Y-%m-%dT%H:%M:%S'

# from qmah.processing import process_vstations as pv
# df_vstations = df_vstations[df_vstations['vstation'] == '100001A']
# df_vstations['polygon'] = 'kattfjordeidet_large'
def process_vstations(domain, vstations_csv_file, output_dir,
                      datetime_start, datetime_end, dt=timedelta(hours=6), 
                      aspects={"F":"", "N": "1", "E":"2", "S":"3", "W":"4"},
                      ncpus=mp.cpu_count()-2,
                      **kwargs):
    """Generate GEOJSON files with critical snowpack stability indices for each vstation
    
    This function processes all virtual stations (vstations) listed in the given CSV file 
    to generate GEOJSON files containing critical snowpack stability indices for each vstation. 
    Each GEOJSON file corresponds to a specific timestamp and contains information that can 
    be used for display on a dashboard or further analysis. The processing involves 
    reading the .pro files for each vstation, extracting relevant stability indices, and assembling 
    hazard and layer characteristics.

    The function is optimized to open each .pro file only once per vstation, extracting the necessary 
    information for all timestamps in a single pass. This optimization reduces file I/O overhead 
    but makes the loop logic more complex. The processing is parallelized over all vstations.

    Parameters
    ----------
    domain : str
        The domain for which the processing is being conducted. Crucial to find the relevant
        snow profile data, see aw-data-provider.snowsim_provider, which can also be leveraged 
        from outside of AWSOME.
    vstations_csv_file : str
        Path to the CSV file containing information about virtual stations, including 
        their spatial coordinates, regions, elevation bands, etc. See 
        https://gitlab.com/avalanche-warning/snow-cover/model-chains/gridded-chain/-/wikis/Gridded-SNOWPACK-simulations
        for more information or hints as to where to find them.
    output_dir : str
        Directory where the output GEOJSON files will be saved.
    datetime_start : datetime
        The start datetime for the data processing.
    datetime_end : datetime
        The end datetime for the data processing.
    dt : timedelta, optional
        Time interval between each timestamp (default is 6 hours).
    aspects : dict, optional
        Dictionary mapping aspect names to codes.
    ncpus : int, optional
        Number of CPU cores to use for parallel processing (default is the number of available cores minus two).
    **kwargs
        Additional keyword arguments passed to internal functions. E.g.,
        - indices (list[str]): which stability indices to compute? All indices listed in qmah.instability.thresholds.py
          can be used.  
        - datetag_dict (dict): datetag information to trigger the computation of a simplified stratigraphy (see also qmah.datetags.datetags)
        - print_progress (bool): Print progress at every 10th % increment together with elapsed time and ETA?

    Returns
    -------
    None
        This function does not return any value. It outputs GEOJSON files to the specified output directory.

    """
    df_vstations = pd.read_csv(vstations_csv_file)
    geojson_timestamps = np.arange(datetime_start, datetime_end, dt).astype(datetime)
    
    """Initialize new columns for layer and hazard characteristics (dictionaries for each aspect)"""
    # hazard_properties = {"problem_type": "storm_slab", "index1": 0.9, "depth_index1": 70, 
    #                "instability_driven_by": ["2024-01-01"], "depth_driven_by": ["2024-01-01"]}
    # hazard_properties2 = {"problem_type": "persistent", "layer_depth": 0.75, "grain_type": "FC", "grain_size": 3, "natural_release": "true", "date_tag": ["2024-01-01"]}
    # scalar_entry = {"value": -999, "depth": -999}
    # layer_properties  = {"sk38_rta": scalar_entry.copy(), "rta": scalar_entry.copy(), "ccl": scalar_entry.copy(), "Punstable": scalar_entry.copy()}
    # hazard_dict = {"flat": [hazard_properties, hazard_properties2], "north": [], "east": [], "south": [], "west": []}
    # layer_dict  = {"flat": layer_properties.copy(), "north": layer_properties.copy(), "east": layer_properties.copy(), "south": layer_properties.copy(), "west": layer_properties.copy()}
    

    df_vstations["snp_characteristics"]  = df_vstations.apply(lambda row: {}, axis=1)
    if 'datetag_dict' in kwargs:
        df_vstations["simplifiedStratigraphy"]  = df_vstations.apply(lambda row: {}, axis=1)
        df_vstations["hazardChartElement"]      = df_vstations.apply(lambda row: {}, axis=1)
    # df_vstations["hazard_characteristics"] = df_vstations.apply(lambda row: hazard_dict, axis=1)
    cols_to_drop = ["easting","northing","i","j", "error","_date", "date", "time", "filename"]
    df_vstations = df_vstations.drop(columns=[colname for colname in cols_to_drop if colname in df_vstations.columns])
    df_vstations["n_vstations_max"] = df_vstations.shape[0]
    df_vstations.reset_index(inplace=True, drop=True)
    dfs = [df_vstations.copy() for ts in geojson_timestamps]
    
    """Calculate indices for all timestamps"""
    # - Process each line in vstations.csv (each grid point) in parallel - #
    # - "child_return" is a list of lists with rows of the initial dataframe df_vstations. Outer list is vstations, inside list is for each geojson timestamp - #
    progress_counter = mp.Manager().Value('i', 0)
    lock = mp.Manager().Lock()
    stime = time.time()
    pbar = {"progress_counter": progress_counter, "lock": lock, "stime": stime, "ntasks": df_vstations.shape[0]}
    kwargs['pbar'] = pbar

    args_list = []
    for row in df_vstations.itertuples(index=False):
        args = (row._asdict(), domain, aspects, geojson_timestamps, kwargs)
        args_list.append(args)

    with mp.Pool(processes=ncpus) as pool:
        child_return = pool.starmap(_safe_extract_from_vstation, args_list)

    for row in range(0,len(dfs[0])): 
        for idf, df in enumerate(dfs):
            if child_return[row] is not None:
                df.loc[row,:] = child_return[row][idf]

    """Generate geojson files"""
    os.makedirs(output_dir, exist_ok=True)
    for ts, df in zip(geojson_timestamps, dfs):
        df = df.drop(columns=[colname for colname in ["index","n_vstations_max"] if colname in df.columns])
        gdf = df_to_geojson(df)    
        json_file_path = os.path.join(output_dir, ts.strftime("%Y-%m-%d_%H-%M-%S") + ".json")
        with open(json_file_path, 'w') as gdf_file:
            json.dump(gdf, gdf_file, indent=4)
    

def _safe_extract_from_vstation(row, domain, aspects, geojson_timestamps, kwargs):
    try:
        return extract_from_vstation(row, domain, aspects, geojson_timestamps, **kwargs)
    except Exception as e:
        print(f"[E]  Error processing {row['vstation']}: {type(e).__name__} - {e}")
        return None

# import configparser
# from qmah.processing import process_vstations as pv
# domain = 'tromso-full'
# configfile = os.path.expanduser(f"~snowpack/forecasts/input/forecast_runtime_{domain}.ini")
# config = configparser.ConfigParser()
# config.read(configfile)
# df_vstations = pd.read_csv(config.get('Paths', '_vstations_csv_file_runtime'))
# geojson_timestamps = np.arange(datetime(2024, 4, 30), datetime(2024,5,30), timedelta(hours=6)).astype(datetime)
# #row = df_vstations[df_vstations["vstation"] == "100110A"].iloc[0].to_dict()
# row = df_vstations[df_vstations["vstation"] == "100001A"].iloc[0].to_dict()
# row['polygon'] = 'kattfjordeidet_large'
# with open('/opt/snowpack/qmah/tmp/wx_datetags/datetags_kattfjordeidet_large.json', 'r') as json_file:
#     dtags = json.load(json_file)
# rows = pv.extract_from_vstation(row, 'tromso-full', {"flat":"", "N": "1", "E":"2", "S":"3", "W":"4"}, geojson_timestamps, datetag_dict=dtags)
def extract_from_vstation(row, domain, aspects, geojson_timestamps, datetag_dict=None, indices=['sk38_rta', 'ccl', 'lwc', 'rta', 'Punstable'], pbar=None):
    """Extract relevant data for multiple GEOJSON files (at different time steps) from a single vstation
    
    Parameters:
        row (dict): a row of df_vstations converted to dict.
                    Note, that if a datetag_dict is provided, the keys listed under datetag_dict['meta'] need to be present in row.
                    See also qmah.datetags.datetags.postprocess_for_datetags(), which can be used to create the datetag_dict.
        domain (str): 
            The domain to locate the corresponding .pro and .smet files.
        aspects (dict): 
            A dictionary mapping aspect names to their corresponding codes
        geojson_timestamps (list of datetime): 
            A list of datetime objects representing the timestamps for which data is extracted and processed.
        datetag_dict (dict, optional): 
            Dictionary of datetag information to trigger the computation of a simplified stratigraphy (see also qmah.datetags.datetags)
        indices (list of str, optional): 
            A list of stability indices to compute and include in the output. All indices listed in qmah.instability.thresholds.py
            can be used.  

    Returns:
        list of dict: 
            A list of dictionaries, where each dictionary corresponds to a GEOJSON file's data at a specific 
            timestamp, containing extracted and computed snowpack characteristics and indices.
    """

    if aspects is None:
        aspects = {None: None}
        if row["aspect"] in ["NW","NE"]:
            row["aspect"] = [row["aspect"],"N"]
        elif row["aspect"] in ["SW","SE"]:
            row["aspect"] = [row["aspect"],"S"]
        toolchain = "snowobs-chain"
    else:
        toolchain = "gridded-chain"

    """Global computations valid for entire vstation and all timesteps"""
    if datetag_dict is not None:
        dtags = np.asarray(datetag_dict[row[datetag_dict['meta']['level1']]][row[datetag_dict['meta']['level2']]], dtype='datetime64[D]')
    
    """Loop through all aspects and time steps"""
    rows = [copy.deepcopy(row) for ts in geojson_timestamps]
    for akey, anumber in zip(aspects.keys(), aspects.values()):

        if toolchain == 'gridded-chain':
            pro_file_path = os.path.join(awsnp.snowsim_dir(domain, toolchain), "VIR" + row["vstation"] + anumber + ".pro")
        else:
            pro_file_path = os.path.join(awsnp.snowsim_dir(domain, toolchain), row["vstation"] + ".pro")
        
        if os.path.exists(pro_file_path) and ('error' not in row or row['error'] == 0):
            """Read .pro file and do computations for one aspect and all time steps"""
            profs, profs_meta_dict = snowpro.read_pro(pro_file_path)
            if "Punstable" in indices:
                profs = calc_punstable(profs, timestamps=geojson_timestamps, verbose=False)

            for irow, ts in zip(rows, geojson_timestamps):
                if ts not in profs.keys():
                    continue
                
                snp_characteristics = {}
                if "density" in profs[ts].keys(): # - Check if snow cover exists - #
                    """Compute necessary parameters for each time step"""
                    if "rta" in indices:
                        profs[ts]["rta"] = profs[ts]["RTA"]
                    if "sk38_rta" in indices:
                        # TODO: compute RTA yourself instead of relying on SNOWPACK output
                        profs[ts]["sk38_rta"]   = np.where(profs[ts]["RTA"] >= instability.thresholds["rta"], profs[ts]["Sk38"], 6)  # set sk38 to 6 (stable) when rta is lower than threshold
                    if "ccl" in indices:
                        profs[ts]["rho_slab"]   = instability.processbased_indices.compute_rho_slab(profs[ts]["density"], profs[ts]["thickness"])
                        profs[ts]["ccl"]        = instability.processbased_indices.compute_ccl_richter2019(profs[ts]["rho_slab"], profs[ts]["density"], profs[ts]["grain size (mm)"], profs[ts]["snow shear strength (kPa)"])
                    # Punstable was computed outside of this loop (see above) for efficiency reasons
                    # TODO: lwc index, time_to_failure, p_unstable_natural, p_unstable_wet_natural
                    
                    """Extract necessary parameters for snowpack characteristics (entire snow column)"""
                    # dict_keys(['height', 'density', 'temperature', 'lwc', 'dendricity (1)', 'sphericity (1)', 'bond size (mm)',
                    # 'grain size (mm)', 'grain type (Swiss Code F1F2F3)', 'graintype, grain size (mm), and density (kg m-3) of SH at surface', 
                    # 'ice volume fraction (%)', 'air volume fraction (%)', 'stress in (kPa)', 'thermal conductivity (W K-1 m-1)', 
                    # 'viscous deformation rate', 'Sn38', 'Sk38', 'hand hardness', 'optical equivalent grain size (mm)', 'snow shear strength (kPa)', 
                    # 'RTA', 'critical cut length (m)', 'thickness', 'bottom', 'graintype'])
                    snp_characteristics["hs"]         = np.round(profs[ts]["height"][-1], 2)
                    # TODO: hn24, hn72
                    mask_new_snow = np.array([any(grain in ['PP', 'DF'] for grain in grains) for grains in profs[ts]['graintype']])
                    snp_characteristics["hnPPDF"]     = np.round(profs[ts]["thickness"][mask_new_snow].sum())
                    for stabindex in indices:
                        snp_characteristics[stabindex] = hazard.instability_depth.extract_stability_and_depth(profs[ts], stabindex)

                    """Extract necessary parameters for simplified stratigraphy (datetag subsets)"""
                    # TODO: We probably only need this for gridded, so its fine here. Otherwise we could follow the same approach as for snp_characteristics
                    if datetag_dict is not None:
                        if "simplifiedStratigraphy" not in irow:
                            irow["simplifiedStratigraphy"] = {}
                        if akey not in irow["simplifiedStratigraphy"]:
                            irow["simplifiedStratigraphy"][akey] = {}
                        # TODO: include user config to subset which datetags to produce output for (last two months, custom array, only when beyond any threshold, etc?)
                        irow["simplifiedStratigraphy"][akey] = assemble_simplified_stratigraphy(profs[ts], indices, dtags)

                        if "hazardChartElement" not in irow:
                            irow["hazardChartElement"] = {}
                        if akey not in irow["hazardChartElement"]:
                            irow["hazardChartElement"][akey] = {}
                        irow["hazardChartElement"][akey] = hazard.hazard_charts.compute_hazard_chart_element_per_aspect(irow["simplifiedStratigraphy"][akey])
                
                else:
                    snp_characteristics["hs"] = 0

                """Assign snp_characteristics to aspect (if necessary, for example for gridded-chain)"""
                if "snp_characteristics" not in irow:
                    irow["snp_characteristics"] = {}
                
                if akey is None:
                    irow["snp_characteristics"] = snp_characteristics
                else:
                    if akey not in irow["snp_characteristics"]:
                        irow["snp_characteristics"][akey] = snp_characteristics

    """Finish"""
    if pbar is not None:
        progress_bar(pbar['progress_counter'], pbar['lock'], pbar["stime"], pbar['ntasks'])
                                
    return rows

def assemble_simplified_stratigraphy(prof, indices, dtags):
    """Assemble simplified stratigraphy for one profile (e.g., specific aspect)
    
    Parameters:
        prof (dict): profile dict for a single timestep (snowpro format)
        indices (list[str]): stability indices to compute. For possible choices, see extract_from_vstation() or qmah.instability.thresholds
        dtags (np.ndarray[str]): datetags that define the simplified stratigraphy, as strings, 'YYYY-MM-DD', 'NaT', and 'new_snow' are allowed.

    Returns:
        simplified_stratigraphy_at_aspect (dict): dict as used in final geojson file.
    """
    prof["datetag"] = datetags.assign_ddates_to_datetags(prof["element deposition date (ISO)"], dtags)
    datetags_iterable = np.unique(prof["datetag"]).tolist()
    datetags_iterable.insert(len(datetags_iterable), 'new_snow')
    simplified_stratigraphy_at_aspect = {}
    for stabindex in indices:
        si_dict = {}
        for dtag in datetags_iterable:
            dtag_filter = f"np.isin(prof['datetag'], '{dtag}')"
            if dtag == 'new_snow':
                dtag_filter = "np.array([any(grain in ['PP', 'DF'] for grain in grains) for grains in prof['graintype']])"
            elif dtag == 'NaT':
                dtag = "shallow"
            sdg = hazard.instability_depth.extract_stability_and_depth(prof, stabindex, subset=dtag_filter, add_gtype=True)
            if sdg is not None:
                si_dict[dtag] = sdg
        simplified_stratigraphy_at_aspect[stabindex] = si_dict
    
    return simplified_stratigraphy_at_aspect

def df_to_geojson(df):
    """Turn a dataframe containing point data into a geojson formatted python dictionary
    
    Parameters
    ----------
    df: dataframe
        the dataframe to convert to geojson
    """

    """list of columns in the dataframe to turn into geojson feature properties"""
    properties = df.columns.to_list()
    properties = [e for e in properties if e not in ("lon","lat","elev")]

    # - Create a new python dict to contain our geojson data, using geojson format - #
    geojson = {'type':'FeatureCollection', 'features':[]}

    # - Loop through each row in the dataframe and convert each row to geojson format - #
    for _, row in df.iterrows():
        # - Create a feature template to fill in - #
        feature = {'type':'Feature',
                   'properties':{},
                   'geometry':{'type':'Point',
                               'coordinates':[]}}

        # - Fill in the coordinates - #
        feature['geometry']['coordinates'] = [row["lon"],row["lat"],row["elev"]]

        # - For each column, get the value and add it as a new feature property - # 
        for prop in properties:
            feature['properties'][prop] = row[prop]
        
        # - Add this feature (aka, converted dataframe row) to the list of features inside our dict - #
        geojson['features'].append(feature)
    
    return geojson


def progress_bar(progress_counter, lock, stime, total_tasks):
    with lock:
        pbar_interval = 5 # %
        progress_counter.value += 1
        if total_tasks <= 100/pbar_interval:
            if progress_counter.value % 2 == 0:
                # print(f"[p]  Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} - {progress_counter.value}/{total_tasks} tasks completed.")
                progress = progress_counter.value / total_tasks
                elapsed = time.time() - stime
                eta = (elapsed / progress) * (1 - progress)

                # Convert elapsed and ETA to hours, minutes, and seconds
                elapsed_hrs, elapsed_rem = divmod(elapsed, 3600)
                elapsed_min, elapsed_sec = divmod(elapsed_rem, 60)
                eta_hrs, eta_rem = divmod(eta, 3600)
                eta_min, eta_sec = divmod(eta_rem, 60)
                print(f"[p]  Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} - " + 
                    f"Progress: {progress:.0%} - " +
                    f"Elapsed: {int(elapsed_hrs):02d}:{int(elapsed_min):02d}:{int(elapsed_sec):02d} - " +
                    f"ETA: {int(eta_hrs):02d}:{int(eta_min):02d}:{int(eta_sec):02d} (hh:mm:ss)")
        else:
            if (progress_counter.value % (total_tasks // (100/pbar_interval))) == 0 or progress_counter.value == total_tasks or progress_counter.value == 1:
                progress = progress_counter.value / total_tasks
                elapsed = time.time() - stime
                eta = (elapsed / progress) * (1 - progress)

                # Convert elapsed and ETA to hours, minutes, and seconds
                elapsed_hrs, elapsed_rem = divmod(elapsed, 3600)
                elapsed_min, elapsed_sec = divmod(elapsed_rem, 60)
                eta_hrs, eta_rem = divmod(eta, 3600)
                eta_min, eta_sec = divmod(eta_rem, 60)

                # Progress bar
                total_hashtags = int(100/pbar_interval)
                hashtag_str = "#" * int(np.ceil(progress * total_hashtags))
                minus_str = "-" * int((1 - progress) * total_hashtags)

                print(f"[p]  |{hashtag_str}{minus_str}| Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')} - Progress: {progress*100:05.2f}% - Elapsed: {int(elapsed_hrs):02d}:{int(elapsed_min):02d}:{int(elapsed_sec):02d} - ETA: {int(eta_hrs):02d}:{int(eta_min):02d}:{int(eta_sec):02d} (hh:mm:ss)", flush=True)


if __name__ == "__main__":
    
    print("[i]  Command-line usage only supported in AWSOME context.")
    from awset import get

    parser = argparse.ArgumentParser(description="Process vstations for a given domain.")
    parser.add_argument("domain", type=str, help="The domain for which to process vstations (e.g., 'domain_name').")
    parser.add_argument("datetag_json", nargs="?", type=str, default=None, help="Path to JSON file containing datetag information (optional).")
    parser.add_argument("--dt_start", type=lambda s: datetime.strptime(s, "%Y-%m-%d %H:%M" if " " in s else "%Y-%m-%d"), default=None, help="Start datetime in the format 'YYYY-MM-DD' or 'YYYY-MM-DD HH:MM' (default: None).")
    parser.add_argument("--dt_end", type=lambda s: datetime.strptime(s, "%Y-%m-%d %H:%M" if " " in s else "%Y-%m-%d"), default=None, help="End datetime in the format 'YYYY-MM-DD' or 'YYYY-MM-DD HH:MM' (default: None).")
    parser.add_argument("--ncpus", type=int, default=mp.cpu_count()-2, help="Number of CPUs to use for processing (default: all CPUs minus 2).")
    args = parser.parse_args()
    domain = args.domain
    dt_start = args.dt_start
    dt_end = args.dt_end
    ncpus = args.ncpus
    datetag_dict = None
    if args.datetag_json:
        with open(args.datetag_json, 'r') as file:
            datetag_dict = json.load(file)

    import configparser
    configfile = os.path.expanduser(f"~snowpack/gridded-chain/input/runtime_{domain}.ini")
    config = configparser.ConfigParser()
    config.read(configfile)

    if dt_start is None or dt_end is None:
        if dt_start is None:
            dt_start = datetime.strptime(get(['gridded-chain', 'run', 'season_start'], domain, throw=True), "%Y-%m-%d")
        if dt_end is None:
            season_end = get(['gridded-chain', 'run', 'season_end'], domain, throw=True)
            date_opera = get(['gridded-chain', 'run', 'date_opera'], domain, throw=True)
            if date_opera.upper() == 'TODAY':
                date_opera = datetime.today().strftime("%Y-%m-%d")
            dt_end = min(datetime.strptime(season_end, "%Y-%m-%d"),
                        datetime.strptime(date_opera, "%Y-%m-%d"))

    process_vstations(
        domain,
        config.get('Paths', '_vstations_csv_file'),
        config.get('Paths', '_geojson_dir'),
        dt_start,
        dt_end,
        ncpus=ncpus,
        datetag_dict=datetag_dict
    )