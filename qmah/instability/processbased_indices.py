import numpy as np

def compute_ccl_richter2019(density_slab, density, gsize, shear_strength):
    """Compute critical crack length [m] based on the parametrization by Richter et al (2019)"""

    rho_ice = 917. # [kg m-3]
    gs_0 = 0.00125 # [m]
    a = 4.6e-9
    b = -2

    ## Eprime = E' = E/(1-nu**2) ; poisson ratio nu=0.2:
    eprime = 5.07e9*(density_slab/rho_ice)**5.13 / (1-0.2**2)
    ## D_sl/sigma_n = D_sl / (rho_sl*9.81*D_sl) = 1/(9.81*rho_sl):
    dsl_over_sigman = 1 / (9.81 * density_slab)
    ## Parametrization Richter (2019):
    #  convert: gsize [mm] -> [m]; shear_strength [kPa] -> [Pa]
    ccl = np.round(
        np.sqrt(a*(density/rho_ice * gsize*0.001/gs_0)**b) * np.sqrt(2*shear_strength*1000 * eprime*dsl_over_sigman),
        decimals=2
        )
    return ccl

def compute_rho_slab(density, thickness):
    """
    Compute density of the slab (rho_slab)
    
    Efficiently vectorized for array of all profile layers; slab refers to all layers above a specific layer

    Parameters:
        density: density of snow layers [kg m**-3]; deepest layer comes first, uppermost layer is last
        thickness: corresponding thickness

    Returns:
        rho_slab: array of slab densities (overlaying each layer)

    Examples:
        >>> density = np.array([300, 150, 60, 30])
        >>> thickness = np.array([10, 10, 10, 10])
        >>> compute_rho_slab(density, thickness)
        array([80., 45., 30., nan])
    """
    rho = density*thickness
    rho_slab = np.round(
        np.append(np.flip(np.cumsum(rho[::-1])/np.cumsum(thickness[::-1]))[1:len(rho)], np.nan),
        decimals=1
        )
    return rho_slab
