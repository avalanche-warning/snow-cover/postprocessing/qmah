thresholds = {
    "Punstable":    0.77,
    "rta":          0.8,
    "sk38":         1,
    "sk38_rta":     1,
    "ccl":          0.4,
    "lwc":          0.3
}

weak_means = {
    "Punstable" :   'max',
    "rta"       :   'max',
    "sk38":         'min',
    "sk38_rta":     'min',
    "ccl":          'min',
    "lwc":          'max'
}

relevant_gtypes = {
    "Punstable" :   ['PP', 'DF', 'FC', 'FCxr', 'DH', 'SH'],
    "rta"       :   ['FC', 'FCxr', 'DH', 'SH'],
    "sk38":         ['FC', 'FCxr', 'DH', 'SH'],
    "sk38_rta":     ['FC', 'FCxr', 'DH', 'SH'],
    "ccl":          ['FC', 'FCxr', 'DH', 'SH'],
    "lwc":          ['MF', 'MFcr', 'FC', 'FCxr', 'DH', 'SH', 'PP', 'DF', 'RG']
}